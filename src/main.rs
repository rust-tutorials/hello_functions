
fn main() {
    println!("Hello, functions!");

    sum_and_display(42, 666);
    println!("  Sum returned: {}", sum(3, 666));

//fn panic_button() -> ! {
//    panic!("Freakout \\o/");
//}

//    panic_button();
//    let panic_var = panic_button();



// The three kinds of parameter: borrow, borrow-mutate and take
    #[derive(Debug)]
    struct Point {
        x: i64,
        y: i64,
    }
    let mut p = Point { x: 2, y: -2};
    println!("  point: {:?}", p);

    fn borrow_and_return(p: &Point) {
        println!("  Borrowing and returning: {:?}", p);
    }
    borrow_and_return(&p);

    fn borrow_and_mutate(p: &mut Point) {
        p.y = 7;
        println!("  Mutating and returning: {:?}", p);
    }
    borrow_and_mutate(&mut p);

    fn take_and_consume(p: Point) {
        println!("  Consuming and destroying: {:?}", p);
    }
    take_and_consume(p);
    // p is no longer owned or accessable by main


    // The three kinds of self: me, myself and I
    impl Point {
        fn reference_self(&self) {
            println!("  Reference Self: {:?}", self);
        }
        fn mutate_self(&mut self) {
            self.x = -1;
            println!("  Mutate Self: {:?}", self);
        }
        fn consume_self(self) {
            println!("  Consume Self: {:?}", self);
        }
    }
    let mut unit = Point { x: 1, y: 1};
    unit.reference_self();
    unit.mutate_self();
    unit.consume_self();

    impl Point {
        fn reference_chain(&self) -> Point {
            println!("  Reference Chain: {:?}", self);
            Point{ x: self.x, y: self.y}
        }
        fn mutate_chain(&mut self) -> Point {
            self.x = self.x - 1;
            println!("  Mutate Chain: {:?}", self);
            Point{ x: self.x, y: self.y}
        }
        fn consume_chain(self) -> Point {
            println!("  Consume Chain: {:?}", self);
            self
        }
    }
    let chain = Point { x: -1, y: -1};
    chain.reference_chain().mutate_chain().consume_chain().mutate_chain().mutate_chain();
    chain.reference_chain(); // back to square one

    impl Point {
        fn new() -> Point {
            Point{ x: 0, y: 0 }
        }
        fn new_xy(x: i64, y: i64) -> Point {
            Point{ x: x, y: y }
        }
    }
    Point::new().reference_chain();
    Point::new_xy(5, 5).reference_chain();


    // Closures
    fn  not_a_closure    (x: i64) -> i64 {x}
    let full_closure   = |x: i64| -> i64 {x};
    let simple_closure = |x: i64|         x ;
    println!("  from Functions to Closures: {} {} {}", not_a_closure(1), full_closure(2), simple_closure(3));


}



fn sum_and_display(x: i64, y: i64) {
    println!("  Sum in func: {}", x + y);
}

fn sum(x: i64, y: i64) -> i64 {
    x + y
}
